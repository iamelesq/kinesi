package mock

import (
	"time"

	"github.com/elesq/kinesi/pkg/models"
)

var mockUser = &models.User{
	ID:      1,
	Name:    "Alice",
	Email:   "alice@sample.com",
	Created: time.Now(),
	Active:  true,
}

type UserModel struct{}

func (u *UserModel) Insert(name, email, password string) error {
	switch email {
	case "dupe@sample.com":
		return models.ErrDuplicateEmail
	default:
		return nil
	}
}

func (u *UserModel) Authenticate(email, password string) (int, error) {
	switch email {
	case "alice@sample.com":
		return 1, nil
	default:
		return 0, models.ErrInvalidCredentials
	}
}

func (u *UserModel) Get(id int) (*models.User, error) {
	switch id {
	case 1:
		return mockUser, nil
	default:
		return nil, models.ErrNoRecord
	}
}
