package mysql

import (
	"database/sql"
	"errors"

	"github.com/elesq/kinesi/pkg/models"
)

type SnippetModel struct {
	DB *sql.DB
}

// Insert adds a new snippet to the database. The input is sanitized against
// a SQL statement using placeholders for passed values. Exec is called and
// passed the parameters. When no error is encountered .LastInsertId() is
// called to get the id of the row that was inserted. The default return is
// int64 so the result is converted to int and returned.
func (m *SnippetModel) Insert(title, content, expires string) (int, error) {
	result, err := m.DB.Exec(insertSnippetSQL, title, content, expires)

	if err != nil {
		return 0, err
	}

	id, err := result.LastInsertId()

	if err != nil {
		return 0, err
	}

	// returned id is of type int64, convert to int before running.
	return int(id), nil
}

// Get executes a query that accepts an id as a parameter and passes that
// to scan to parse values and populate the models.Snippet object that was
// created. DB.QueryRow errors are deferred until scan is called. If the
// query returns no rows Scan() generates a sql.ErrNoRows error will return
// a sql.ErrNorows error which is handled explicitly. If no error is thrown
// the models.Snippet object is returned.
func (m *SnippetModel) Get(id int) (*models.Snippet, error) {
	s := &models.Snippet{}

	err := m.DB.QueryRow(findOneSnippetById, id).Scan(&s.ID, &s.Title, &s.Content, &s.Created, &s.Expires)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) { // when query returned no rows
			return nil, models.ErrNoRecord // uses models error for complete encapsulation
		} else {
			return nil, err
		}
	}

	return s, nil
}

// Latest runs a multi row query and iterates over the result using the
// Scan() call to parse attributes into a empty models.Snippet object
// and appends them to the slice. On completion of the iterations it
// checks for errors and returns if errors are found. When no errors
// it returns the slice containing the records.
func (m *SnippetModel) Latest() ([]*models.Snippet, error) {
	rows, err := m.DB.Query(findTenLatestRecords)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	snippets := []*models.Snippet{}

	for rows.Next() {
		s := &models.Snippet{}
		err = rows.Scan(&s.ID, &s.Title, &s.Content, &s.Created, &s.Expires)
		if err != nil {
			return nil, err
		}
		snippets = append(snippets, s)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return snippets, nil
}
