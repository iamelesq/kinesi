package mysql

const (

	// snippets
	insertSnippetSQL = "INSERT INTO snippets (title, content, created, expires) VALUES (?, ?, UTC_TIMESTAMP(), DATE_ADD(UTC_TIMESTAMP(), INTERVAL ? DAY))"

	findOneSnippetById = "SELECT id, title, content, created, expires FROM snippets WHERE expires > UTC_TIMESTAMP() AND id = ?"

	findTenLatestRecords = "SELECT id, title, content, created, expires FROM snippets WHERE expires > UTC_TIMESTAMP() ORDER BY created DESC LIMIT 10"

	// users
	insertUserSQL = "INSERT INTO users (name, email, hashed_password, created) VALUES (?,?,?,UTC_TIMESTAMP())"

	findUserHashedPassword = "SELECT id, hashed_password FROM users WHERE email = ? and active = true"

	findOneUserById = "SELECT id, name, email, created, active FROM users WHERE id = ?"
)
