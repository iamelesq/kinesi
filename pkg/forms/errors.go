package forms

type errors map[string][]string

// appends a new error against a field.
func (e errors) Add(field, message string) {
	e[field] = append(e[field], message)
}

// returns the first error for a given field if any exist
func (e errors) Get(field string) string {
	es := e[field]
	if len(es) == 0 {
		return ""
	}

	return es[0]
}
