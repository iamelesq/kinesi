# Kinesi

Notes and snippets application written using Go1.16

### Packages

Uses:
- [Pat router](https://github.com/bmizerany/pat) `go get github.com/bmizerany/pat`
- [Session manager golangcolleges/sessions](https://github.com/golangcollege/sessions) `github.com/golangcollege/sessions`
- [Alice middleware](https://github.com/justinas/alice) `go get github.com/justinas/alice`
- [MYSQL driver for Go](https://github.com/go-sql-driver/mysql) `go get github.com/go-sql-driver/mysql`

### h2 and TLS

Project uses local self-signed certificates. These are not stored in source control but can be easily generated using Go's crypto package source. On Mac this can be done by:
```shell
# create a tls folder at the root of your project
mkdir tls
cd tls

# run the script to generate tls certs, replacing the X.X.X with your installed Go version number.
go run /usr/local/Cellar/go/X.X.X/libexec/src/crypto/tls/generate_cert.go --rsa-bits=2048 --host=localhost
```