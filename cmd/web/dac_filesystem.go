package main

import (
	"net/http"
	"path/filepath"
)

type DACFileSystem struct {
	fs http.FileSystem
}

// Open is a directory access controlled filesystem hotrodded method
// for our server so that we can block areas of the fileServer we
// do not want users to access such as the static directory.
// files and directory listings.
func (dac DACFileSystem) Open(path string) (http.File, error) {
	f, err := dac.fs.Open(path)
	if err != nil {
		return nil, err
	}

	s, _ := f.Stat()
	if s.IsDir() {
		index := filepath.Join(path, "index.html")
		_, err := dac.fs.Open(index)
		if err != nil {

			closeErr := f.Close()
			if closeErr != nil {
				return nil, closeErr
			}

			return nil, err
		}
	}

	return f, nil
}
