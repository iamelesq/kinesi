package main

// functions and convenience function to centralize the error handling
// efforts and separate concerns. Added benefit of approach is that we
// can reduce the amount of noisy, repetitive code in our logic.

import (
	"bytes"
	"fmt"
	"net/http"
	"runtime/debug"
	"time"

	"github.com/justinas/nosurf"
)

// Appends the stacktrace of the current go routine to the log.
func (app *application) serverError(w http.ResponseWriter, err error) {
	trace := fmt.Sprintf("%s\n%s", err.Error(), debug.Stack())
	app.errorLog.Output(2, trace)

	http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
}

// Generates a human-friendly text representation for a given http error
func (app *application) clientError(w http.ResponseWriter, status int) {
	app.infoLog.Printf("in clientError. status is: %d", status)
	http.Error(w, http.StatusText(status), status)
}

// Convenience wrapper around clientError for one specific case. The not found 404 error
func (app *application) notFound(w http.ResponseWriter) {
	app.infoLog.Println("in notFound")
	app.clientError(w, http.StatusNotFound)
}

// adds default data to the template data object
func (app *application) addDefaultdata(td *templateData, r *http.Request) *templateData {
	if td == nil {
		td = &templateData{}
	}

	// add the CSRFToken
	td.CSRFToken = nosurf.Token(r)

	// adds the current year
	td.CurrentYear = time.Now().Year()

	// adds any flash message to template data
	td.Flash = app.session.PopString(r, "flash")

	// adds authentication status
	td.IsAuthenticated = app.isAuthenticated(r)

	return td
}

// helper function that fetches the template from the templateCache for the application
// and performs a two-stage process of trial rendering to a buffer, if there is an error
// then it handles the error and sends to the user. Where no error exists it writes the
// contents of the buffer to io.Writer or our http.ResponseWriter.
func (app *application) render(w http.ResponseWriter, r *http.Request, name string, td *templateData) {
	// fetch appropriate template from cache based on name
	ts, ok := app.templateCache[name]
	if !ok {
		app.serverError(w, fmt.Errorf("template %s does not exist", name))
		return
	}

	// new buffer
	buf := new(bytes.Buffer)

	err := ts.Execute(buf, app.addDefaultdata(td, r))
	if err != nil {
		app.infoLog.Println("error executing to buffer")
		app.serverError(w, err)
		return
	}

	// write contents to the writer
	buf.WriteTo(w)
}

func (app *application) isAuthenticated(r *http.Request) bool {
	isAuthenticated, ok := r.Context().Value(contextKeyIsAuthenticated).(bool)
	if !ok {
		return false
	}

	return isAuthenticated
}
