package main

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"github.com/elesq/kinesi/pkg/forms"
	"github.com/elesq/kinesi/pkg/models"
)

// home is the default or landing route for the site. Calls app.snippets.Latest()
// to execute a query and get the latest snippets. Calls render passing in the
// page to be rendered and the data.
func (app *application) home(w http.ResponseWriter, r *http.Request) {
	s, err := app.snippets.Latest()
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.render(w, r, "home.page.tmpl", &templateData{
		Snippets: s,
	})
}

// Parses the request to determine the id that was passed. It calls the app.snippets.Get()
// method to execute a query to find the row with the passed id from the DB. The data
// returned by the query is wrapped as the data object that will be passed into the template.
// The Execute() method is called on the io.Writer or http.ResponseWriter and passing the data.
func (app *application) showSnippet(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(r.URL.Query().Get(":id"))
	if err != nil || id < 1 {
		app.notFound(w)
		return
	}

	// uses the snippetModel object's Get to retrieve data
	// when not fond return a 404
	s, err := app.snippets.Get(id)
	if err != nil {
		if errors.Is(err, models.ErrNoRecord) {
			app.notFound(w)
		} else {
			app.serverError(w, err)
		}
		return
	}

	app.render(w, r, "show.page.tmpl", &templateData{
		Snippet: s,
	})
}

// createSnippetForm renders the page that allows the user to create and
// post a new snippet
func (app *application) createSnippetForm(w http.ResponseWriter, r *http.Request) {
	app.render(w, r, "create.page.tmpl", &templateData{
		Form: forms.New(nil),
	})
}

// createSnippet adds a new snippet.
func (app *application) createSnippet(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	form := forms.New(r.PostForm)
	form.Required("title", "content", "expires")
	form.MaxLength("title", 100)
	form.PermittedValues("expires", "365", "7", "1")

	// if form is not valid redisplay with errors
	if !form.Valid() {
		app.render(w, r, "create.page.tmpl", &templateData{Form: form})
		return
	}

	id, err := app.snippets.Insert(form.Get("title"), form.Get("content"), form.Get("expires"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	// adds data attribute and value to the session
	app.session.Put(r, "flash", "snippet successfully created!")

	http.Redirect(w, r, fmt.Sprintf("/snippet/%d", id), http.StatusSeeOther)
}

// renders the empty signup form.
func (app *application) signupUserForm(w http.ResponseWriter, r *http.Request) {
	app.render(w, r, "signup.page.tmpl", &templateData{
		Form: forms.New(nil),
	})
}

// processes the posted signup form. If the for is not valid the user is
// returned to the form to complete correctly. In the event the user that
// is signing up is using an existing email address then they are advised
// that it already exists.
// the processing also validates the provided email against a valid email
// regex and ensures the password passes the minimum length check test.
func (app *application) signupUser(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	form := forms.New(r.PostForm)
	form.Required("name", "email", "password")
	form.MaxLength("name", 255)
	form.MaxLength("email", 255)
	form.MatchesPattern("email", forms.EmailRegex)
	form.MinLength("password", 6)

	if !form.Valid() {
		app.render(w, r, "signup.page.tmpl", &templateData{Form: form})
		return
	}

	err = app.users.Insert(form.Get("name"), form.Get("email"), form.Get("password"))
	if err != nil {
		if errors.Is(err, models.ErrDuplicateEmail) {
			form.Errors.Add("email", "This email address is already in use")
			app.render(w, r, "signup.page.tmpl", &templateData{
				Form: form,
			})
		} else {
			app.serverError(w, err)
		}
		return
	}

	// success case, add flash to session
	app.session.Put(r, "flash", "Sign up successful. You can now login")

	// redirect
	http.Redirect(w, r, "/user/login", http.StatusSeeOther)
}

// renders the login form for a user to enter email and password.
func (app *application) loginUserForm(w http.ResponseWriter, r *http.Request) {
	app.render(w, r, "login.page.tmpl", &templateData{
		Form: forms.New(nil),
	})
}

// processes the login request by retrieving the hashed password for the
// user. If the matching check does not pass then a generic error is thrown
// if the match is successful the session is updated to reflect the user is
// logged in and the user is redirected to the add snippet page.
func (app *application) loginUser(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	form := forms.New(r.PostForm)
	id, err := app.users.Authenticate(form.Get("email"), form.Get("password"))
	if err != nil {
		if errors.Is(err, models.ErrInvalidCredentials) {
			form.Errors.Add("generic", "Email or password incorrect")
			app.render(w, r, "login.page.tmpl", &templateData{Form: form})
		} else {
			app.serverError(w, err)
		}
		return
	}

	// add the id of the current user to the session, so they are logged in
	app.session.Put(r, "authenticatedUserId", id)

	// redirect
	http.Redirect(w, r, "/snippet/create", http.StatusSeeOther)
}

// checks there is a logged in user, if so the user is logged out by
// removing the userId from the session and a flash message is presented
// to the user and they are redirected to the application home. If there
// is no logged in user at the time a flash message is presented with
// a no user logged in message.
func (app *application) logoutUser(w http.ResponseWriter, r *http.Request) {
	id := app.session.Get(r, "authenticatedUserId")
	if id == nil {
		app.session.Put(r, "flash", "No user was logged in")
	} else {

		app.session.Remove(r, "authenticatedUserId")
		app.session.Put(r, "flash", "You've been successfully logged out")
	}
	http.Redirect(w, r, "/", http.StatusSeeOther)
}

func ping(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("ok"))
}
