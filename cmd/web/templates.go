package main

import (
	"html/template"
	"path/filepath"
	"time"

	"github.com/elesq/kinesi/pkg/forms"
	"github.com/elesq/kinesi/pkg/models"
)

type templateData struct {
	CSRFToken       string
	CurrentYear     int
	Flash           string
	Form            *forms.Form
	IsAuthenticated bool
	Snippet         *models.Snippet
	Snippets        []*models.Snippet
}

// returns a nicely formatted human friendly formatted string
// representation of a time.Time object.
func humanDate(t time.Time) string {
	if t.IsZero() {
		return ""
	}

	return t.UTC().Format("02 Jan 2006 at 15:04")
}

var functions = template.FuncMap{
	"humanDate": humanDate,
}

func newTemplateCache(dir string) (map[string]*template.Template, error) {
	// init map that acts as cache
	cache := map[string]*template.Template{}

	// use the filepath.glob to get a slice of all files
	// with *.page.tmpl (all pages in app)
	pages, err := filepath.Glob(filepath.Join(dir, "*.page.tmpl"))
	if err != nil {
		return nil, err
	}

	// loop through pages
	for _, page := range pages {
		name := filepath.Base(page)

		// template.FuncMap must be registered with the template set
		// before parsing files.
		ts, err := template.New(name).Funcs(functions).ParseFiles(page)
		if err != nil {
			return nil, err
		}

		// ParseGlob to get details of any layout
		ts, err = ts.ParseGlob(filepath.Join(dir, "*.layout.tmpl"))
		if err != nil {
			return nil, err
		}

		// ParseGlob to get details of any partials
		ts, err = ts.ParseGlob(filepath.Join(dir, "*.partial.tmpl"))
		if err != nil {
			return nil, err
		}

		// add template set to cache with pagename as key
		cache[name] = ts
	}

	return cache, nil
}
