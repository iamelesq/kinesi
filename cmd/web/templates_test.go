package main

import (
	"testing"
	"time"
)

func TestHumanData(t *testing.T) {

	tests := []struct {
		name string
		tm   time.Time
		want string
	}{
		{
			name: "UTC",
			tm:   time.Date(2020, 12, 25, 10, 0, 0, 0, time.UTC),
			want: "25 Dec 2020 at 10:00",
		},
		{
			name: "empty",
			tm:   time.Time{},
			want: "",
		},
		{
			name: "CET",
			tm:   time.Date(2020, 12, 25, 10, 0, 0, 0, time.FixedZone("CET", 1*60*60)),
			want: "25 Dec 2020 at 09:00",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			hd := humanDate(test.tm)

			if hd != test.want {
				t.Errorf("want: %q, got: %q\n", test.want, hd)
			}
		})
	}
}
